<?php

namespace App\Http\Controllers\api\Branch;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Http\Resources\FormatPostResource;

class BranchController extends Controller
{
    public function getBranch()
    {
        $dataBranch = Branch::all();

        return [
            'dataBranch' => $dataBranch,
        ];
    }

    public function createBranch(Request $request)
    {
        $validatedData = $request->validate([
            'location_id' => 'required',
            'name' => 'required|unique:branches,name',
            'contact' => 'required',
        ]);

        $user = Branch::create([
            'location_id' => $request->location_id,
            'name' => $request->name,
            'contact' => $request->contact,
        ]);
    }

    public function deleteBranch($id)
    {
        $dataBranch = Branch::findOrFail($id);
        $dataBranch->delete();
    }
}
