<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Register</div>

                    <div class="card-body">
							<form action="{{ url('register/process') }}" method="POST">
                            @csrf

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" id="phone" name="phone" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="nik">NIK</label>
                                <input type="text" id="nik" name="nik" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="domicile">Domicile</label>
                                <input type="text" id="domicile" name="domicile" class="form-control" required>
                            </div>
                            
                            <div class="form-group">
                                <label for="instance">Instance</label>
                                <select name="instance" id="instance" class="form-control">
                                    @if($dataInstance)
                                        @foreach($dataInstance as $instance)
                                            <option value="{{ $instance->id }}">{{ $instance->name }}</option>
                                        @endforeach
                                    @else
                                        <option value="">No instances available</option>
                                    @endif
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Register</button>
                            <a href="{{ url('') }}" class="btn btn-success">Login</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
