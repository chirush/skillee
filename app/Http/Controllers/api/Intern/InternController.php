<?php

namespace App\Http\Controllers\api\Intern;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Intern;
use App\Http\Resources\FormatPostResource;

class InternController extends Controller
{
    public function createIntern(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:interns,email',
            'password' => 'required',
            'phone' => 'required|unique:interns,phone',
            'nik' => 'required|unique:interns,nik',
            'domicile' => 'required',
            'instance' => 'required',
        ]);

        $user = Intern::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone' => $request->phone,
            'nik' => $request->nik,
            'domicile' => $request->domicile,
            'instance_id' => $request->instance,
        ]);
    }
}
