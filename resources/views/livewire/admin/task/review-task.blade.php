<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Review Task</h1>
    <form wire:submit.prevent="reviewTask">
        <div class="mb-4">
            <label for="intern_name" class="block text-gray-700 font-semibold mb-2">Intern</label>
            <input wire:model.lazy="intern_name" type="text" id="intern_name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" disabled>
            @error('intern_name') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="title" class="block text-gray-700 font-semibold mb-2">Title</label>
            <input wire:model.lazy="title" type="text" id="title" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter title" disabled>
            @error('title') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Description</label>
            <textarea wire:model.lazy="description" rows="4" id="description" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter description" disabled></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="report" class="block text-gray-700 font-semibold mb-2">Report</label>
            <textarea wire:model.lazy="report" rows="4" id="report" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter report" disabled></textarea>
            @error('report') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="file" class="block text-gray-700 font-semibold mb-2">File</label>
            <a href="{{ asset('storage/task/' . $file) }}" target="_blank" class="text-blue-500">See File</a>
            @error('report') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="file" class="block text-gray-700 font-semibold mb-2">Picture</label>
            <a href="{{ asset('storage/task/' . $picture) }}" target="_blank" class="text-blue-500">See Picture</a>
            @error('report') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="status" class="block text-gray-700 font-semibold mb-2">Status</label>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="status" value="Revised" class="form-radio text-blue-500">
                    <span class="ml-2">Revise</span>
                </label>
            </div>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="status" value="Completed" class="form-radio text-blue-500">
                    <span class="ml-2">Complete</span>
                </label>
            </div>
            @error('status') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @if ($status == 'Revised')
        <div class="mb-4">
            <label for="revise_note" class="block text-gray-700 font-semibold mb-2">Revise Note</label>
            <textarea wire:model.lazy="revise_note" rows="4" id="revise_note" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter revise note"></textarea>
            @error('revise_note') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @endif
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Save</button>
        </div>
    </form>
</main>
