<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Update Application Status</h1>
        <div class="mb-6">
            <h2 class="text-xl font-semibold mb-2">Detail Job</h2>
            <div class="mb-4">
                <label for="position" class="block text-gray-700 font-semibold mb-2">Position</label>
                <input type="text" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" value="{{ $dataJobs->name }}" disabled>
            </div>
            <div class="mb-4">
                <label for="branch" class="block text-gray-700 font-semibold mb-2">Branch</label>
                <input type="text" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" value="{{ $dataBranches->name }}" disabled>
            </div>
            <h2 class="text-xl font-semibold mb-2">Detail Intern</h2>
            <div class="mb-4">
            <div class="container mx-auto">
                <div class="w-full bg-white shadow-md rounded-lg overflow-hidden">
                    <div class="bg-gray-200 px-6 py-4 flex items-center justify-between">
                        <div>
                            <h2 class="text-xl font-semibold text-gray-800">{{ $dataIntern->name }}</h2>
                        </div>
                        <div class="flex items-center">
                            @if(!$dataIntern->picture)
                            <img src="https://www.gravatar.com/avatar/?d=mp" alt="Profile Picture" class="w-12 h-12 rounded-full">
                            @else
                            <img src="{{ asset('storage/picture') }}/{{ $dataIntern->picture }}" alt="Profile Picture" class="w-12 h-12 rounded-full">
                            @endif
                        </div>
                    </div>

                    <div class="flex">
                        <div class="w-2/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">Kontak Intern</h3>
                            </div>
                            <form wire:submit.prevent="updateYourContact" id="yourContactForm">
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Email</span><br/>
                                    <span id="yourEmailDisplay">{{ $dataIntern->email }}</span>
                                </p>
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Phone</span><br/>
                                    <span id="yourPhoneDisplay">{{ $dataIntern->phone }}</span>
                                </p>
                            </form>
                        </div>
                        <div class="w-1/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">
                                    Kontak Pembimbing
                                    @if (!$dataIntern->mentor_email || !$dataIntern->mentor_phone)
                                    <span class="text-red-500">*</span>
                                    @endif
                                </h3>
                            </div>
                            <form wire:submit.prevent="updateMentorContact" id="mentorContactForm">
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Nama</span><br/>
                                    <span id="mentorEmailDisplay">{{ $dataIntern->mentor_name ?: '-' }}</span>
                                </p>
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Email</span><br/>
                                    <span id="mentorEmailDisplay">{{ $dataIntern->mentor_email ?: '-' }}</span>
                                </p>
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Phone</span><br/>
                                    <span id="mentorPhoneDisplay">{{ $dataIntern->mentor_phone ?: '-' }}</span>
                                </p>
                            </form>
                        </div>
                    </div>
                    <div class="flex">
                        <div class="w-2/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">Personal Information</h3>
                            </div>
                            <form wire:submit.prevent="updatePersonalInfo" id="personalInfoForm">
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">NIK</span><br/>
                                    <span id="nikDisplay">{{ $dataIntern->nik }}</span>
                                </p>
                                <p class="text-gray-600 mb-2">
                                    <span class="font-semibold">Jenis Kelamin</span><br/>
                                    <span id="genderDisplay">{{ $dataIntern->gender ?: '-' }}</span>
                                </p>
                                <p class="text-gray-600">
                                    <span class="font-semibold">Domisili</span><br/>
                                    <span id="domicileDisplay">{{ $formattedDomicile }}</span>
                                </p>
                            </form>
                        </div>
                        <div class="w-1/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">Pendidikan</h3>
                            </div>
                            <form wire:submit.prevent="updateEducation">
                                <p class="text-gray-600 mb-2"><span class="font-semibold">
                                @if ($dataIntern->level == "High School")
                                Sekolah</span><br/><span id="schoolDisplay">{{ DB::table('instances')->where('id', $dataIntern->instance_id)->value('name') }}</span></p>
                                @else
                                Universitas</span><br/><span id="schoolDisplay">{{ DB::table('instances')->where('id', $dataIntern->instance_id)->value('name') }}</span></p>
                                <p class="text-gray-600 mb-2"><span class="font-semibold">Fakultas</span><br/><span id="facultyDisplay">{{ $dataIntern->faculty }}</span></p>
                                @endif
                                <p class="text-gray-600"><span class="font-semibold">Jurusan</span><br/><span id="programDisplay">{{ $dataIntern->program }}</span></p>
                            </form>
                        </div>
                    </div>
                    <div class="flex mb-4">
                        <div class="w-2/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">
                                    Bio
                                    @if (!$dataIntern->bio)
                                    <span class="text-red-500">*</span>
                                    @endif
                                </h3>
                            </div>
                            <p id="bioDisplay" class="text-gray-600">{{ $dataIntern->bio ?: '-' }}</p>
                            <form id="bioForm" wire:submit.prevent="updateBio" class="hidden">
                                <textarea name="bio" wire:model="bio" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">{{ $dataIntern->bio }}</textarea>
                                <button type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Save</button>
                            </form>
                        </div>

                        <div class="w-1/3 bg-white p-6">
                            <div class="mb-4 flex items-center justify-between">
                                <h3 class="text-lg font-semibold text-gray-800">
                                    Resume
                                </h3>
                            </div>
                            <div id="resumeDisplay" class="{{ $dataIntern->resume ? '' : 'hidden' }} mt-6">
                                    <a href="{{ asset('storage/resumes') }}/{{ $dataIntern->resume }}" target="_blank" class="bg-gray-800 hover:bg-gray-700 text-white py-2 px-4 rounded">View Resume</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <form wire:submit.prevent="updateApplication">
        <div class="mb-4">
            <label for="status" class="block text-gray-700 font-semibold mb-2">Status</label>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="status" value="Approved" class="form-radio text-blue-500">
                    <span class="ml-2">Approved</span>
                </label>
            </div>
            <div>
                <label class="inline-flex items-center">
                    <input type="radio" wire:model.lazy="status" value="Declined" class="form-radio text-blue-500">
                    <span class="ml-2">Declined</span>
                </label>
            </div>
            @error('status') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @if ($status == 'Approved')
        <div class="mb-4">
            <label for="instructor" class="block text-gray-700 font-semibold mb-2">Instructor</label>
            <select wire:model.lazy="instructor" id="instructor" name="instructor" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
                <option value="null" selected disabled>{{ __('Select instructor') }}</option>
                @foreach ($dataInstructor as $instructor)
                <option value="{{ $instructor->id }}">{{ $instructor->name }}</option>
                @endforeach
            </select>
            @error('instructor') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        @endif
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Save</button>
        </div>
    </form>
</main>

<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        var notification = document.getElementById("notification");
        if (notification) {
            notification.style.display = "block";

            setTimeout(function() {
                notification.style.display = "  ";
            }, 5000);
        }
    });
</script>
