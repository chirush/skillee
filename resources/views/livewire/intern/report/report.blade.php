<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Laporan</h1>
    <div class="mb-6 flex justify-between items-center">
        <div class="flex items-center">
            <label for="month" class="mr-2">Bulan:</label>
            <select id="month" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm mr-4">
            @php
            $months = [
                'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 
                'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
            ];
            $currentMonth = date('n');
            @endphp

            @foreach($months as $index => $month)
                <option value="{{ $month }}" @if($index + 1 == $currentMonth) selected @endif>{{ $month }}</option>
            @endforeach
            </select>
            <label for="year" class="mr-2">Tahun:</label>
            <select id="year" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
                @foreach(range(date('Y') - 1, date('Y') + 4) as $year)
                    <option value="{{ $year }}" @if($year == date('Y')) selected @endif>{{ $year }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <a href="{{ url('/internship/reports/add') }}" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Tambah Laporan</a>
        </div>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">Tanggal</th>
                    <th class="px-4 py-2">Judul</th>
                    <th class="px-4 py-2">Deskripsi</th>
                    <th class="px-4 py-2">Foto</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            @php
            use Carbon\Carbon;
            Carbon::setLocale('id');
            @endphp

            <tbody class="text-gray-700">
                @foreach($dataReports as $report)
                <tr>
                    <td class="border px-4 py-2">{{ Carbon::parse($report->date)->translatedFormat('d F Y') }}</td>
                    <td class="border px-4 py-2">{{ $report->title }}</td>
                    <td class="border px-4 py-2">{{ $report->description }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ asset('storage/report/' . $report->picture) }}" target="_blank" class="text-blue-500">Lihat Foto</a>
                    </td>
                    <td class="border px-4 py-2">
                        <a href="{{ url('/internship/reports/edit/' . $report->id) }}" class="text-blue-500">Edit</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[1, 10, 20, 50], [1, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        var currentMonth = $('#month').val().toLowerCase();
        var currentYear = $('#year').val().toLowerCase();

        table.column(0).search(currentYear).draw();
        table.column(0).search(currentMonth).draw();

        $('#status').on('change', function() {
            var status = $(this).val().toLowerCase();
            if (status === "all") {
                table.column(3).search('').draw();
            } else {
                table.column(3).search(status).draw();
            }
        });

        $('#year').on('change', function() {
            var year = $(this).val().toLowerCase();
            table.column(0).search(year).draw();
        });

        $('#month').on('change', function() {
            var month = $(this).val().toLowerCase();
            table.column(0).search(month).draw();
        });
    });
</script>
@endscript