<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\User\UserController;
use App\Http\Controllers\api\Instance\InstanceController;
use App\Http\Controllers\api\Intern\InternController;

Route::GET('/user', [UserController::class, 'getAllUser'])->middleware('auth:sanctum');
Route::POST('/add-user', [UserController::class, 'createUser']);

Route::GET('/instance', [InstanceController::class, 'getInstance']);

Route::POST('/add-intern', [InternController::class, 'createIntern']);
