<?php

namespace App\Livewire\Admin\User;

use Livewire\Component;
use App\Models\User as Users;

class User extends Component
{
    public $dataUsers;

    public function mount()
    {
        $this->dataUsers = Users::leftJoin('branches', 'users.branch_id', '=', 'branches.id')
        ->select('users.*', 'branches.name as branch_name')
        ->get();
    }

    public function render()
    {
        return view('livewire.admin.user.user', [
            'dataUsers' => $this->dataUsers,
        ])->layout('livewire.layout.admin');
    }
}
