<div class="container mx-auto py-8">
    <h1 class="text-3xl font-semibold text-center mb-8">Contact Us</h1>
    <div class="max-w-md mx-auto bg-white p-8 rounded-md shadow-md">
        <form>
            <div class="mb-4">
                <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Name</label>
                <input type="text" id="name" name="name" class="form-input w-full border-gray-300 rounded-md">
            </div>
            <div class="mb-4">
                <label for="email" class="block text-gray-700 text-sm font-bold mb-2">Email</label>
                <input type="email" id="email" name="email" class="form-input w-full border-gray-300 rounded-md">
            </div>
            <div class="mb-4">
                <label for="message" class="block text-gray-700 text-sm font-bold mb-2">Message</label>
                <textarea id="message" name="message" rows="4" class="form-textarea w-full border-gray-300 rounded-md"></textarea>
            </div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded">Submit</button>
        </form>
    </div>
</div>