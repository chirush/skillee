<?php

namespace App\Livewire\Intern\Report;

use Livewire\Component;
use App\Models\Report;
use App\Models\Branch;

class EditReport extends Component
{
    public $reportId;
    public $title;
    public $description;
    public $date;

    public function mount($id)
    {
        $this->reportId = $id;
        $report = Report::findOrFail($id);
        $this->title = $report->title;
        $this->description = $report->description;
        $this->date = $report->date;

        $this->branches = Branch::all();
    }

    public function render()
    {
        return view('livewire.intern.report.edit-report', [
            'reportId' => $this->reportId,
        ])->layout('livewire.layout.internship');
    }
}
