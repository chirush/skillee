<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Magang Telkom</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.11/dist/css/splide.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css">
    <link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
</head>
<body class="bg-no-repeat bg-cover bg-center" style="background-image: url({{ asset('img/bg.png') }});">
    <nav class="p-4 bg-transparent">
        <div class="container mx-auto flex justify-between items-center">
            <div class="flex items-center ml-8">
                <div class="font-semibold">
                    <a href="{{ url('/') }}" wire:navigate class="text-2xl text-white">SKILLEE</a>
                </div>
                <div class="hidden md:flex md:space-x-4 ml-8 text-white">
                    <a href="#home" class="hover:text-gray-300 font-semibold text-sm" onclick="scrollToTarget(event)">Beranda</a>
                    <a href="#about" class="hover:text-gray-300 font-semibold text-sm" onclick="scrollToTarget(event)">Tentang</a>
                    <a href="#contact" class="hover:text-gray-300 font-semibold text-sm" onclick="scrollToTarget(event)">Kontak</a>
                </div>
            </div>
            <div class="hidden md:flex text-white mr-8">
                <a href="{{ url('/login') }}" class="bg-blue-600 hover:bg-blue-700 text-white font-semibold py-2 px-4 rounded-lg text-sm flex items-center">
                    Masuk
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" id="login" class="h-5 w-5 fill-current"><g><path d="M19 4h-2a1 1 0 0 0 0 2h1v12h-1a1 1 0 0 0 0 2h2a1 1 0 0 0 1-1V5a1 1 0 0 0-1-1zm-7.2 3.4a1 1 0 0 0-1.6 1.2L12 11H4a1 1 0 0 0 0 2h8.09l-1.72 2.44a1 1 0 0 0 .24 1.4 1 1 0 0 0 .58.18 1 1 0 0 0 .81-.42l2.82-4a1 1 0 0 0 0-1.18z"></path></g></svg>
                </a>

            </div><div class="md:hidden">
    <button id="menu-toggle" class="text-white focus:outline-none">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16m-7 6h7"/>
        </svg>
    </button>
    <div id="mobile-menu" class="hidden absolute top-16 right-0 bg-white shadow-md z-20 w-48 rounded-md">
        <a href="{{ url('/') }}" wire:navigate class="block py-2 px-4 text-gray-800 hover:bg-gray-100">Home</a>
        <a href="{{ url('/about-us') }}" wire:navigate class="block py-2 px-4 text-gray-800 hover:bg-gray-100">About us</a>
        <a href="#contact" class="block py-2 px-4 text-gray-800 hover:bg-gray-100">Contact</a>
        <a href="{{ url('/login') }}" wire:navigate class="block py-2 px-4 text-gray-800 hover:bg-gray-100">Login</a>
    </div>
</div>

        </div>
    </nav>

    <main class="flex-1">
    {{ $slot }}
    </main>

    <footer class="bg-white text-black py-4">
        <div class="container mt-[40px] mx-auto text-center text-sm">
            <p>&copy; 2024 Telkom Graha Cianjur. All rights reserved.</p>
        </div>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.11/dist/js/splide.min.js"></script>
    <script src="{{ asset('js/homepage.js') }}"></script>

    @livewireScripts

</body>
</html>