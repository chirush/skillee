<?php

namespace App\Livewire\Intern;

use Livewire\Component;
use Session;
use App\Models\Jobs;
use Illuminate\Support\Facades\DB;

class Dashboard extends Component
{
    public function mount()
    {
        $this->dataJobs = Jobs::join('branches', 'jobs.branch_id', '=', 'branches.id')
        ->select('jobs.*', 'branches.name as branch_name')
        ->get();
    }

    public function render()
    {
        return view('livewire.intern.dashboard', [
            'dataJobs' => $this->dataJobs,
        ])->layout('livewire.layout.internship');
    }
}
