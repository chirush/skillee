document.getElementById('pictureInput').addEventListener('change', function() {
    document.getElementById('pictureForm').submit();
});
document.getElementById('resumeInput').addEventListener('change', function() {
    document.getElementById('resumeForm').submit();
});

function toggleResumeForm() {
    var resumeDisplay = document.getElementById('resumeDisplay');
    var resumeForm = document.getElementById('resumeForm');
    var editResumeBtn = document.getElementById('editResumeBtn');
    if (resumeDisplay.classList.contains('hidden')) {
        resumeDisplay.classList.remove('hidden');
        resumeForm.classList.add('hidden');
        editResumeBtn.textContent = 'Edit';
    } else {
        resumeDisplay.classList.add('hidden');
        resumeForm.classList.remove('hidden');
        editResumeBtn.textContent = 'Cancel';
    }
}

function toggleBioForm() {
    var bioDisplay = document.getElementById('bioDisplay');
    var bioForm = document.getElementById('bioForm');
    var editBioBtn = document.getElementById('editBioBtn');
    if (bioDisplay.classList.contains('hidden')) {
        bioDisplay.classList.remove('hidden');
        bioForm.classList.add('hidden');
        editBioBtn.textContent = 'Edit';
    } else {
        bioDisplay.classList.add('hidden');
        bioForm.classList.remove('hidden');
        editBioBtn.textContent = 'Cancel';
    }
}

function toggleEducationForm() {
    var schoolDisplay = document.getElementById('schoolDisplay');
    var facultyDisplay = document.getElementById('facultyDisplay');
    var programDisplay = document.getElementById('programDisplay');
    var schoolForm = document.getElementById('schoolForm');
    var facultyForm = document.getElementById('facultyForm');
    var programForm = document.getElementById('programForm');
    var educationForm = document.getElementById('educationForm');
    var editEducationBtn = document.getElementById('editEducationBtn');
    var saveEducationBtn = document.getElementById('saveEducationBtn');
    if (schoolDisplay.classList.contains('hidden')) {
        schoolDisplay.classList.remove('hidden');
        facultyDisplay.classList.remove('hidden');
        programDisplay.classList.remove('hidden');
        schoolForm.classList.add('hidden');
        facultyForm.classList.add('hidden');
        programForm.classList.add('hidden');
        saveEducationBtn.classList.add('hidden');
        editEducationBtn.textContent = 'Edit';
    } else {
        schoolDisplay.classList.add('hidden');
        facultyDisplay.classList.add('hidden');
        programDisplay.classList.add('hidden');
        schoolForm.classList.remove('hidden');
        facultyForm.classList.remove('hidden');
        programForm.classList.remove('hidden');
        saveEducationBtn.classList.remove('hidden');
        editEducationBtn.textContent = 'Cancel';
    }
}

function togglePersonalInfoForm() {
    var nikDisplay = document.getElementById('nikDisplay');
    var genderDisplay = document.getElementById('genderDisplay');
    var domicileDisplay = document.getElementById('domicileDisplay');
    var nikForm = document.getElementById('nikForm');
    var genderForm = document.getElementById('genderForm');
    var domicileForm = document.getElementById('domicileForm');
    var editPersonalInfoBtn = document.getElementById('editPersonalInfoBtn');
    var savePersonalInfoBtn = document.getElementById('savePersonalInfoBtn');
    
    if (nikDisplay.classList.contains('hidden')) {
        nikDisplay.classList.remove('hidden');
        genderDisplay.classList.remove('hidden');
        domicileDisplay.classList.remove('hidden');
        nikForm.classList.add('hidden');
        genderForm.classList.add('hidden');
        domicileForm.classList.add('hidden');
        savePersonalInfoBtn.classList.add('hidden');
        editPersonalInfoBtn.textContent = 'Edit';
    } else {
        nikDisplay.classList.add('hidden');
        genderDisplay.classList.add('hidden');
        domicileDisplay.classList.add('hidden');
        nikForm.classList.remove('hidden');
        genderForm.classList.remove('hidden');
        domicileForm.classList.remove('hidden');
        savePersonalInfoBtn.classList.remove('hidden');
        editPersonalInfoBtn.textContent = 'Cancel';
    }
}

function toggleMentorContactForm() {
    var mentorNameDisplay = document.getElementById('mentorNameDisplay');
    var mentorEmailDisplay = document.getElementById('mentorEmailDisplay');
    var mentorPhoneDisplay = document.getElementById('mentorPhoneDisplay');
    var mentorNameForm = document.getElementById('mentorNameForm');
    var mentorEmailForm = document.getElementById('mentorEmailForm');
    var mentorPhoneForm = document.getElementById('mentorPhoneForm');
    var editMentorContactBtn = document.getElementById('editMentorContactBtn');
    var saveMentorContactBtn = document.getElementById('saveMentorContactBtn');

    if (mentorEmailDisplay.classList.contains('hidden')) {
        mentorNameDisplay.classList.remove('hidden');
        mentorEmailDisplay.classList.remove('hidden');
        mentorPhoneDisplay.classList.remove('hidden');
        mentorNameForm.classList.add('hidden');
        mentorEmailForm.classList.add('hidden');
        mentorPhoneForm.classList.add('hidden');
        saveMentorContactBtn.classList.add('hidden');
        editMentorContactBtn.textContent = 'Edit';
    } else {
        mentorNameDisplay.classList.add('hidden');
        mentorEmailDisplay.classList.add('hidden');
        mentorPhoneDisplay.classList.add('hidden');
        mentorNameForm.classList.remove('hidden');
        mentorEmailForm.classList.remove('hidden');
        mentorPhoneForm.classList.remove('hidden');
        saveMentorContactBtn.classList.remove('hidden');
        editMentorContactBtn.textContent = 'Cancel';
    }
}

function toggleYourContactForm() {
    var yourNameDisplay = document.getElementById('yourNameDisplay');
    var yourEmailDisplay = document.getElementById('yourEmailDisplay');
    var yourPhoneDisplay = document.getElementById('yourPhoneDisplay');
    var yourNameForm = document.getElementById('yourNameForm');
    var yourEmailForm = document.getElementById('yourEmailForm');
    var yourPhoneForm = document.getElementById('yourPhoneForm');
    var editYourContactBtn = document.getElementById('editYourContactBtn');
    var saveYourContactBtn = document.getElementById('saveYourContactBtn');

    if (yourEmailDisplay.classList.contains('hidden')) {
        yourNameDisplay.classList.remove('hidden');
        yourEmailDisplay.classList.remove('hidden');
        yourPhoneDisplay.classList.remove('hidden');
        yourNameForm.classList.add('hidden');
        yourEmailForm.classList.add('hidden');
        yourPhoneForm.classList.add('hidden');
        saveYourContactBtn.classList.add('hidden');
        editYourContactBtn.textContent = 'Edit';
    } else {
        yourNameDisplay.classList.add('hidden');
        yourEmailDisplay.classList.add('hidden');
        yourPhoneDisplay.classList.add('hidden');
        yourNameForm.classList.remove('hidden');
        yourEmailForm.classList.remove('hidden');
        yourPhoneForm.classList.remove('hidden');
        saveYourContactBtn.classList.remove('hidden');
        editYourContactBtn.textContent = 'Cancel';
    }
}
