<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('attendances', function (Blueprint $table) {
            $table->id();
            $table->foreignId('intern_id')->unsigned()->references('id')->on('interns')->onDelete('cascade');
            $table->enum('status', ['Hadir', 'Tidak Hadir', 'Izin', 'Sakit', 'Terlambat', 'Pulang Cepat']);
            $table->text('description')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('picture');
            $table->date('date');
            $table->time('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('attendances');
    }
};
