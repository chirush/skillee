<?php

namespace App\Livewire\Admin\Instance;

use Livewire\Component;
use App\Models\Instance;

class AddInstance extends Component
{
    public $name;
    public $description;
    public $level;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|unique:instances,name',
            'description' => 'required',
            'level' => 'required',
        ]);
    }

    public function addInstance()
    {
        $validatedData = $this->validate([
            'name' => 'required|unique:instances,name',
            'description' => 'required',
            'level' => 'required',
        ]);

        $instance = Instance::create([
            'name' => $this->name,
            'description' => $this->description,
            'level' => $this->level,
        ]);

        return redirect('/admin/instances');

    }
    
    public function render()
    {
        return view('livewire.admin.instance.add-instance')->layout('livewire.layout.admin');
    }
}
