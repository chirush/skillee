<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Magang Telkom</title>

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&amp;display=swap" rel="stylesheet">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles

    <style>
        body {
            font-family: 'Poppins', sans-serif;
            display: flex;
            flex-direction: column;
            min-height: 100vh;
            margin: 0;
        }

        main {
            flex: 1;
            display: flex;
            flex-direction: column;
        }

        footer {
            flex-shrink: 0;
        }
    </style>
</head>
<body>
    <main class="flex-1">
    {{ $slot }}
    </main>

    <footer class="bg-gray-800 text-white py-4">
        <div class="container mx-auto text-center">
            <p>&copy; 2024 Telkom Graha Cianjur. All rights reserved.</p>
        </div>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3.6.11/dist/js/splide.min.js"></script>
    <script>
        document.getElementById("menu-toggle").addEventListener("click", function() {
            document.getElementById("mobile-menu").classList.toggle("hidden");
        });

        
        document.addEventListener('DOMContentLoaded', function () {
            var splide = new Splide('#image-carousel', {
                type   : 'loop',
                perPage: 1,
                autoplay: true,
                arrows : false,
            });

            splide.on('mounted move', function () {
                var activeSlide = splide.Components.Elements.slides[splide.index];
                activeSlide.classList.add('animate__animated', 'animate__fadeInRight');
            });

            splide.mount();
        });
    </script>

    @livewireScripts
</body>
</html>