<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Intern\InternController;
use App\Http\Controllers\FileHandlingController;
use App\Livewire\Homepage\Index;
use App\Livewire\Homepage\Login;
use App\Livewire\Homepage\Register;
use App\Livewire\Homepage\Contact;
use App\Livewire\Homepage\ChooseRegistration;
use App\Livewire\intern\Dashboard;
use App\Livewire\intern\Job\Job;
use App\Livewire\intern\Job\Status;
use App\Livewire\intern\Attendance\Attendance;
use App\Livewire\intern\Attendance\AddAttendance;
use App\Livewire\intern\Report\Report;
use App\Livewire\intern\Report\AddReport;
use App\Livewire\intern\Report\EditReport;
use App\Livewire\intern\Task\Task;
use App\Livewire\intern\Task\UpdateTask;
use App\Livewire\intern\Profile\Profile;
use App\Livewire\admin\Location\Location;
use App\Livewire\admin\Location\AddLocation;
use App\Livewire\admin\Location\EditLocation;
use App\Livewire\admin\Monitor\Attendance as AttendanceAdmin;
use App\Livewire\admin\Monitor\Report as ReportAdmin;
use App\Livewire\admin\Branch\Branch;
use App\Livewire\admin\Branch\AddBranch;
use App\Livewire\admin\Branch\EditBranch;
use App\Livewire\admin\instance\Instance;
use App\Livewire\admin\instance\AddInstance;
use App\Livewire\admin\instance\EditInstance;
use App\Livewire\admin\job\Job as JobAdmin;
use App\Livewire\admin\job\AddJob;
use App\Livewire\admin\job\EditJob;
use App\Livewire\admin\task\Task as TaskInstructor;
use App\Livewire\admin\task\AddTask;
use App\Livewire\admin\task\EditTask;
use App\Livewire\admin\task\ReviewTask;
use App\Livewire\admin\user\Intern;
use App\Livewire\admin\user\User;
use App\Livewire\admin\user\AddUser;
use App\Livewire\admin\application\Application;
use App\Livewire\admin\application\UpdateApplication;
use App\Livewire\admin\message\Message;
 
Route::get('/', function () {
    return view('welcome');
});


Route::get('/add-user', function () {
    return view('add_user');
});
Route::POST('/add-user/process', [UserController::class, 'createUser']);

Route::GET('/verify-email/{token}', [AuthController::class, 'verifyEmail'])->name('verify.email');
Route::GET('/resend-verification-email', [AuthController::class, 'resendVerificationEmail'])->name('resend.verification.email');
Route::GET('/logout', [AuthController::class, 'logout']);

//Landing Page
Route::GET('/', Index::class)->name('homepage');
Route::GET('/login', Login::class)->name('login');
Route::get('/register', Register::class)->name('register');
Route::GET('/choose-registration', ChooseRegistration::class)->name('choose-registration');

//Internship
Route::group(['middleware' => 'auth:intern'], function () {
    Route::GET('/internship/dashboard', Dashboard::class)->name('dashboard-internship');
    Route::GET('/internship/jobs', Job::class)->name('job-internship');
    Route::GET('/internship/jobs/status', Status::class)->name('job-status-internship');

    //Attendance
    Route::GET('/internship/attendances', Attendance::class)->name('attendance-internship');
    Route::GET('/internship/attendances/add', AddAttendance::class)->name('add-attendance-internship');
    Route::POST('/internship/attendances/add-process', [FileHandlingController::class, 'addAttendance'])->name('add-attendance-internship-process');

    //Report
    Route::GET('/internship/reports', Report::class)->name('report-internship');
    Route::GET('/internship/reports/add', AddReport::class)->name('add-report-internship');
    Route::POST('/internship/reports/add-process', [FileHandlingController::class, 'addReport'])->name('add-report-internship-process');
    Route::GET('/internship/reports/edit/{id}', EditReport::class)->name('edit-report-internship');
    Route::POST('/internship/reports/edit-process/{id}', [FileHandlingController::class, 'editReport'])->name('edit-report-internship-process');

    //Task
    Route::GET('/internship/tasks', Task::class)->name('task-internship');
    Route::GET('/internship/tasks/update/{id}', UpdateTask::class)->name('update-task-internship');
    Route::POST('/internship/tasks/update-process/{id}', [FileHandlingController::class, 'updateTask'])->name('update-task-internship-process');

    //Profile
    Route::GET('/internship/profile', Profile::class)->name('profile-internship');
    Route::POST('/upload-resume', [FileHandlingController::class, 'uploadResume'])->name('uploadResume');
    Route::POST('/upload-picture', [FileHandlingController::class, 'uploadPicture'])->name('uploadPicture');
});


//Admin

Route::group(['middleware' => 'auth'], function () {
    //Branch
    Route::GET('/admin/branches', Branch::class)->name('branch-admin');
    Route::GET('/admin/branches/add', AddBranch::class)->name('add-branch-admin');
    Route::GET('/admin/branches/edit/{id}', EditBranch::class)->name('edit-branch-admin');

    //Instance
    Route::GET('/admin/instances', Instance::class)->name('instance-admin');
    Route::GET('/admin/instances/add', AddInstance::class)->name('add-instance-admin');
    Route::GET('/admin/instances/edit/{id}', EditInstance::class)->name('edit-instance-admin');

    //Job
    Route::GET('/admin/jobs', JobAdmin::class)->name('job-admin');
    Route::GET('/admin/jobs/add', AddJob::class)->name('add-job-admin');
    Route::GET('/admin/jobs/edit/{id}', EditJob::class)->name('edit-job-admin');

    //Application
    Route::GET('/admin/applications', Application::class)->name('application-admin');
    Route::GET('/admin/applications/update/{id}', UpdateApplication::class)->name('update-application-admin');

    //Message
    Route::GET('/admin/messages', Message::class)->name('message-admin');
    Route::GET('/admin/messages/mark/{id}', [FileHandlingController::class, 'markAsRead'])->name('mark-message-admin');

    //User
    Route::GET('/admin/interns', Intern::class)->name('list-interns');
    Route::GET('/admin/certificate/{id}', [FileHandlingController::class, 'generateCertificate'])->name('generateCertificate');
    Route::GET('/admin/users', User::class)->name('list-user');
    Route::GET('/admin/users/add', AddUser::class)->name('add-user');

    //Monitor
    Route::GET('/admin/attendances', AttendanceAdmin::class)->name('attendance-admin');
    Route::GET('/admin/reports', ReportAdmin::class)->name('report-admin');

    //Task
    Route::GET('/admin/tasks', TaskInstructor::class)->name('task-instructor');
    Route::GET('/admin/tasks/add', AddTask::class)->name('add-task-instructor');
    Route::GET('/admin/tasks/edit/{id}', EditTask::class)->name('edit-task-instructor');
    Route::GET('/admin/tasks/review/{id}', ReviewTask::class)->name('review-task-instructor');
});
