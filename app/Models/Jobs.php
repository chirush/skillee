<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    use HasFactory;
    protected $table = 'jobs';
    protected $primaryKey = 'id';

    protected $fillable = [
        'branch_id', 'name', 'description', 'quota', 'date_start', 'deadline', 'level', 'applicator', 'periode_start', 'periode_end', 'criteria'
    ];
}
