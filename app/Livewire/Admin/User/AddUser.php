<?php

namespace App\Livewire\Admin\User;

use Livewire\Component;
use App\Models\User;
use App\Models\Branch;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AddUser extends Component
{
    public $name;
    public $email;
    public $password;
    public $phone;
    public $role;
    public $branch;

    protected $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required',
        'phone' => 'required|unique:users,phone',
    ];

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function addUser()
    {
        $validatedData = $this->validate();

        if (Auth::user()->role == "Human Resources"){
            $branch_id = Auth::user()->branch_id;
        }else{
            $branch_id = $this->branch;
        }

        User::create([
            'branch_id' => $branch_id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'phone' => $this->phone,
            'role' => $this->role,
        ]);

        return redirect('/admin/users');
    }

    public function render()
    {
        return view('livewire.admin.user.add-user', [
        ])->layout('livewire.layout.admin');
    }
}
