<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Add Instance</h1>
    <form wire:submit.prevent="addInstance">
        <div class="mb-4">
            <label for="name" class="block text-gray-700 font-semibold mb-2">Name</label>
            <input wire:model.lazy="name" type="text" id="name" name="name" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter name">
            @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="description" class="block text-gray-700 font-semibold mb-2">Description</label>
            <textarea wire:model.lazy="description" id="description" name="description" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Enter description"></textarea>
            @error('description') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div class="mb-4">
            <label for="level" class="block text-gray-700 font-semibold mb-2">Level</label>
            <select wire:model.lazy="level" id="level" name="level" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">
                <option value="null" selected disabled>{{ __('Select level') }}</option>
                <option value="High School">High School</option>
                <option value="University">University</option>
            </select>
            @error('level') <span class="text-red-500">{{ $message }}</span> @enderror
        </div>
        <div>
            <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg">Submit</button>
        </div>
    </form>
</main>
