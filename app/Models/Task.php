<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    protected $table = 'tasks';
    protected $primaryKey = 'id';

    protected $fillable = [
        'intern_id', 'title', 'description', 'deadline', 'status', 'report', 'file', 'picture', 'revise_note'
    ];
}
