<?php

namespace App\Livewire\Intern\Job;

use Livewire\Component;
use App\Models\Application as Applications;
use Illuminate\Support\Facades\Auth;

class Status extends Component
{
    public $dataApplications;

    public function mount()
    {
        $indonesianMonths = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        function formatDate($date, $months)
        {
            $dateComponents = explode('-', $date);
            $year = $dateComponents[0];
            $month = intval($dateComponents[1]) - 1;
            $day = intval($dateComponents[2]);
            return $day . ' ' . $months[$month] . ' ' . $year;
        }

        $this->dataApplications = Applications::join('jobs', 'applications.job_id', '=', 'jobs.id')
            ->join('branches', 'jobs.branch_id', '=', 'branches.id')
            ->join('interns', 'applications.intern_id', '=', 'interns.id')
            ->select(
                'applications.*',
                'jobs.name as job_name',
                'jobs.periode_start',
                'branches.name as branch_name',
                'interns.name as intern_name',
                'interns.resume'
            )
            ->where('applications.intern_id', '=', Auth::guard('intern')->user()->id)
            ->get();

        foreach ($this->dataApplications as $application) {
            $application->formatted_periode_start = formatDate($application->periode_start, $indonesianMonths);
            $application->formatted_application_date = formatDate($application->application_date, $indonesianMonths);
        }
    }

    public function deleteApplication($id)
    {
        Applications::find($id)->delete();
        $this->mount();
    }

    public function render()
    {
        return view('livewire.intern.job.status', [
            'dataApplications' => $this->dataApplications,
        ])->layout('livewire.layout.internship');
    }
}
