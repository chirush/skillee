<?php

namespace App\Livewire\Admin\Message;

use Livewire\Component;
use App\Models\Message as Messages;

class Message extends Component
{
    public $dataMessages;

    public function mount()
    {
        $this->dataMessages = Messages::all();
    }

    public function render()
    {
        $this->dispatch('statuses-count-updated');

        return view('livewire.admin.Message.Message', [
            'dataMessages' => $this->dataMessages,
        ])->layout('livewire.layout.admin');
    }
}
