<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Task</h1>
    <div class="mb-6">
        <a href="{{ url('/admin/tasks/add') }}" wire:navigate class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Add Task</a>
    </div>
    <div class="mb-2 text-right">
        <label for="status" class="mr-2">Filter by Status:</label>
        <select id="status" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            <option value="In Progress">In Progress</option>
            <option value="Under Review">Under Review</option>
            <option value="Revised">Revised</option>
            <option value="Completed">Completed</option>
        </select>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">No</th>
                    <th class="px-4 py-2">Intern</th>
                    <th class="px-4 py-2">Title</th>
                    <th class="px-4 py-2">Description</th>
                    <th class="px-4 py-2">Deadline</th>
                    <th class="px-4 py-2">Report</th>
                    <th class="px-4 py-2">File</th>
                    <th class="px-4 py-2">Picture</th>
                    <th class="px-4 py-2">Revise Note</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach ($dataTasks as $index => $tasks)
                <tr>
                    <td class="border px-4 py-2">{{ $index + 1 }}</td>
                    <td class="border px-4 py-2">{{ $tasks->intern_name }}</td>
                    <td class="border px-4 py-2">{{ $tasks->title }}</td>
                    <td class="border px-4 py-2">{{ $tasks->description }}</td>
                    <td class="border px-4 py-2">{{ $tasks->deadline }}</td>
                    <td class="border px-4 py-2">{{ $tasks->report }}</td>
                    <td class="border px-4 py-2">
                        @if ($tasks->file)
                        <a href="{{ asset('storage/task') }}/{{ $tasks->file }}" target="_blank" class="text-blue-500">See File</a>
                        @endif
                    </td>
                    <td class="border px-4 py-2">
                        @if ($tasks->picture)
                        <a href="{{ asset('storage/task') }}/{{ $tasks->picture }}" target="_blank" class="text-blue-500">See Picture</a>
                        @endif
                    </td>
                    <td class="border px-4 py-2">{{ $tasks->revise_note }}</td>
                    <td class="border px-4 py-2">{{ $tasks->status }}</td>
                    <td class="border px-4 py-2">
                        @if ($tasks->status == "Under Review")
                        <a href="{{ url('/admin/tasks/review/' . $tasks->id) }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Review</a>
                        @elseif ($tasks->status == "In Progress")
                        <a href="{{ url('/admin/tasks/edit/' . $tasks->id) }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Edit</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>


@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });

        $('#status').on('change', function() {
            var status = $(this).val().toLowerCase();
            if (status === "all") {
                table.column(9).search('').draw();
            } else {
                table.column(9).search(status).draw();
            }
        });
    });
</script>
@endscript