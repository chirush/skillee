<?php

namespace App\Livewire\Homepage;

use Livewire\Component;

class Contact extends Component
{
    public function render()
    {
        return view('livewire.homepage.contact')->layout('livewire.layout.homepage');
    }
}
