<main class="flex-1 p-6">
    <div class="flex items-center justify-between mb-4">
        <div class="flex-1 mr-2">
            <input type="text" wire:model.debounce.300ms="searchTerm" class="w-full px-4 py-2 border rounded-lg" placeholder="Cari pekerjaan...">
        </div>
        <div class="flex-1 ml-2">
            <select wire:model="selectedBranch" class="w-full px-4 py-2 border rounded-lg">
                <option value="">Semua Cabang</option>
                    @php
                    $allBranch = getAllBranch();
                    @endphp
                @foreach ($allBranch as $branch)
                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="ml-2">
            <button wire:click="search" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">
                Cari    
            </button>
        </div>
    </div>
    <div class="grid grid-cols-1 lg:grid-cols-3 gap-6">
        <div class="col-span-1">
            @foreach($dataJobs as $job)
            <div class="bg-white rounded-md shadow-md p-4 mb-4 cursor-pointer transition duration-300 ease-in-out transform hover:scale-105
                @if($selectedJob && $selectedJob->id === $job->id) border border-blue-500 @endif" 
                wire:click="selectJob({{ $job->id }})">
                <h3 class="text-lg font-semibold text-gray-800">{{ $job->name }}</h3>
                <p class="text-sm text-gray-600 mb-2">{{ $job->branch_name }}</p>
                <div class="flex justify-between items-center mb-2">
                    <div class="flex items-center">
                        <i class="fas fa-user-friends text-gray-600 w-[20px] mr-2"></i>
                        <p class="text-xs text-gray-600"><span class="font-semibold">Kuota:</span> {{ $job->quota }}</p>
                    </div>
                    <div class="flex items-center">
                        <i class="fas fa-file-alt text-gray-600 w-[20px] mr-2"></i>
                        <p class="text-xs text-gray-600"><span class="font-semibold">Pendaftar:</span> {{ $job->applicator }}</p>
                    </div>
                </div>
                <div class="flex items-center">
                    <i class="fas fa-calendar-alt text-gray-600 w-[20px] mr-2"></i>
                    <p class="text-xs text-gray-600"><span class="font-semibold">Batas Waktu:</span> {{ $job->deadline }}</p>
                </div>
            </div>
            @endforeach

            {{ $dataJobs->links() }}
        </div>
        <div class="bg-white rounded-md shadow-md p-6 col-span-2">
            @if($selectedJob)
                <h2 class="text-2xl font-semibold text-gray-800">{{ $selectedJob->name }}</h2>
                <p class="text-md text-gray-600 mb-2">{{ $selectedJob->branch_name }}</p>

                @if ($pendingApplication >= 1)
                    <p class="mt-2 text-md text-red-600">Anda sudah melamar untuk sebuah pekerjaan.</p>
                @else
                    <button wire:click="applyForJob({{ $selectedJob->id }})" class="mt-2 bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg" onclick="confirmApply()">
                        Daftar
                    </button>
                @endif
                <div class="flex items-center mb-2 mt-6">
                    <i class="fas fa-map-marker-alt text-gray-600 w-[20px] mr-2"></i>
                    <p class="text-md text-gray-600"><span class="font-semibold">Alamat</span><br>{{ $selectedJob->address }}</p>
                </div>
                <div class="flex justify-between items-center mb-2">
                    <div class="flex items-center">
                        <i class="fas fa-users text-gray-600 w-[20px] mr-2"></i>
                        <p class="text-md text-gray-600"><span class="font-semibold">Kuota</span><br>{{ $selectedJob->quota }}</p>
                    </div>
                    <div class="flex items-center">
                        <i class="fas fa-calendar text-gray-600 w-[20px] mr-2"></i>
                        <p class="text-md text-gray-600"><span class="font-semibold">Periode</span><br>{{ $selectedJob->periode_start_formatted }} - {{ $selectedJob->periode_end_formatted }}</p>
                    </div>
                </div>
                <div class="flex items-center mb-2">
                    <i class="fas fa-calendar-alt text-gray-600 w-[20px] mr-2"></i>
                    <p class="text-md text-gray-600"><span class="font-semibold">Batas Waktu</span><br>{{ $selectedJob->deadline_formatted }}</p>
                </div>
                <div class="flex items-center mb-2">
                    <i class="fas fa-phone text-gray-600 w-[20px] mr-2"></i>
                    <p class="text-md text-gray-600"><span class="font-semibold">Kontak</span><br>{{ $selectedJob->contact }}</p>
                </div>
                <div class="flex items-center mb-2 mt-4">
                    <p class="text-gray-600 whitespace-pre-line"><span class="font-semibold text-lg">Deskripsi Magang</span><br/><span class="text-md">{{ $selectedJob->description }}</span></p>
                </div>
                <div class="flex items-center mb-2 mt-4">
                    <p class="text-gray-600 whitespace-pre-line"><span class="font-semibold text-lg">Kriteria Peserta</span><br><span class="text-md">{{ $selectedJob->criteria }}</span></p>
                </div>
            @endif
        </div>
    </div>
</main>

<script>
    function confirmApply() {
        if (confirm('Are you sure you want to apply for this job?')) {
            Livewire.emit('applyForJob', {{ $selectedJob->id }});
        }
    }
</script>
