<?php

namespace App\Livewire\Intern\Attendance;

use Livewire\Component;
use App\Models\Attendance as Attendances;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Attendance extends Component
{
    public $dataAttendances;
    public $todayCount;
    public $statusesCountstatusesCount;
    public $selectedMonth;
    public $convertedMonth;

    public function mount()
    {
        $intern = Auth::guard('intern')->user();

        if (!$intern->branch_id || $intern->date_start > now()) {
            return redirect('internship/jobs');
        }

        $internId = Auth::guard('intern')->user()->id;
        Carbon::setLocale('id');
        $timezone = new \DateTimeZone('Asia/Jakarta');
        $todayDate = Carbon::now($timezone)->toDateString();

        $this->selectedMonth = Carbon::now($timezone)->translatedFormat('F');

        $currentMonth = Carbon::now($timezone)->month;

        $this->dataAttendances = Attendances::where('intern_id', $internId)
        ->get();

        $this->todayCount = Attendances::where('intern_id', $internId)
        ->whereDate('created_at', $todayDate)
        ->count();

        $this->statusesCount = [
            'Hadir' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $currentMonth)
            ->where('status', 'Hadir')
            ->count(),

            'Terlambat' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $currentMonth)
            ->where('status', 'Terlambat')
            ->count(),

            'Sakit' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $currentMonth)
            ->where('status', 'Sakit')
            ->count(),

            'Izin' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $currentMonth)
            ->where('status', 'Izin')
            ->count(),

            'Tidak Hadir' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $currentMonth)
            ->where('status', 'Tidak Hadir')
            ->count(),
        ];
    }

    public function fetchStatusesCount()
    {
        $internId = Auth::guard('intern')->user()->id;
        $monthNameToNumber = [
            'Januari' => 1,
            'Februari' => 2,
            'Maret' => 3,
            'April' => 4,
            'Mei' => 5,
            'Juni' => 6,
            'Juli' => 7,
            'Agustus' => 8,
            'September' => 9,
            'Oktober' => 10,
            'November' => 11,
            'Desember' => 12,
        ];

        $this->convertedMonth = $monthNameToNumber[$this->selectedMonth];

        $this->statusesCount = [
            'Hadir' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $this->convertedMonth)
            ->where('status', 'Hadir')
            ->count(),

            'Terlambat' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $this->convertedMonth)
            ->where('status', 'Terlambat')
            ->count(),

            'Sakit' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $this->convertedMonth)
            ->where('status', 'Sakit')
            ->count(),

            'Izin' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $this->convertedMonth)
            ->where('status', 'Izin')
            ->count(),

            'Tidak Hadir' => Attendances::where('intern_id', $internId)
            ->whereMonth('created_at', $this->convertedMonth)
            ->where('status', 'Tidak Hadir')
            ->count(),
        ];
    }

    public function render()
    {
        $this->dispatch('statuses-count-updated', $this->statusesCount);
        
        return view('livewire.intern.attendance.attendance', [
            'dataAttendances' => $this->dataAttendances,
            'todayCount' => $this->todayCount,
            'statusesCount' => $this->statusesCount,
        ])->layout('livewire.layout.internship');
    }
}
