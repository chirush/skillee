@php
    use Carbon\Carbon;
    Carbon::setLocale('id');

    $indonesianmonths = [
        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 
        'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    ];
    $currentMonth = date('n');

    $internId = Auth::guard('intern')->user()->id;
    $hasAttendedToday = hasAttendedToday($internId);
@endphp
<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Kehadiran</h1>
    <div class="mb-6 flex justify-between items-center">
        <div class="flex items-center">
            <label for="month" class="mr-2">Bulan:</label>
            <select id="month" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm mr-4" wire:model="selectedMonth" wire:change="fetchStatusesCount">
            @foreach($indonesianmonths as $index => $month)
                <option value="{{ $month }}" @if($index + 1 == $selectedMonth) selected @endif>{{ $month }}</option>
            @endforeach
            </select>
            <label for="year" class="mr-2">Tahun:</label>
            <select id="year" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
                @foreach(range(date('Y') - 2, date('Y') + 1) as $year)
                    <option value="{{ $year }}" @if($year == date('Y')) selected @endif>{{ $year }}</option>
                @endforeach
            </select>
        </div>
        <div>
            @if (!$hasAttendedToday)
            <a href="{{ url('/internship/attendances/add') }}" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Presensi</a>
            @endif
        </div>
    </div>

    <div class="mb-6 grid grid-cols-1 md:grid-cols-5 gap-4">
        <div class="bg-green-100 p-4 rounded-2xl shadow-sm text-center">
            <h2 class="text-md font-bold">Hadir</h2>
            <p class="text-lg">{{ $statusesCount['Hadir'] }}</p>
        </div>
        <div class="bg-yellow-100 p-4 rounded-2xl shadow-sm text-center">
            <h2 class="text-md font-bold">Terlambat</h2>
            <p class="text-lg">{{ $statusesCount['Terlambat'] }}</p>
        </div>
        <div class="bg-red-100 p-4 rounded-2xl shadow-sm text-center">
            <h2 class="text-md font-bold">Sakit</h2>
            <p class="text-lg">{{ $statusesCount['Sakit'] }}</p>
        </div>
        <div class="bg-blue-100 p-4 rounded-2xl shadow-sm text-center">
            <h2 class="text-md font-bold">Izin</h2>
            <p class="text-lg">{{ $statusesCount['Izin'] }}</p>
        </div>
        <div class="bg-gray-100 p-4 rounded-2xl shadow-sm text-center">
            <h2 class="text-md font-bold">Tidak Hadir</h2>
            <p class="text-lg">{{ $statusesCount['Tidak Hadir'] }}</p>
        </div>
    </div>

    <div class="mb-2 text-right">
        <label for="status" class="mr-2">Filter by Status:</label>
        <select id="status" class="bg-white border border-gray-300 rounded px-2 py-1 text-sm">
            <option value="all">All</option>
            <option value="Hadir">Hadir</option>
            <option value="Sakit">Sakit</option>
            <option value="Izin">Izin</option>
            <option value="Terlambat">Terlambat</option>
        </select>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">Tanggal</th>
                    <th class="px-4 py-2">Jam</th>
                    <th class="px-4 py-2">Deskripsi</th>
                    <th class="px-4 py-2">Status</th>
                </tr>
            </thead>
            @php
            @endphp

            <tbody class="text-gray-700">
                @foreach($dataAttendances as $attendances)
                <tr>
                    <td class="border px-4 py-2">{{ Carbon::parse($attendances->date)->translatedFormat('d F Y') }}</td>
                    <td class="border px-4 py-2">{{ $attendances->time }}</td>
                    <td class="border px-4 py-2">{{ $attendances->description }}</td>
                    <td class="border px-4 py-2">{{ $attendances->status }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>
@script
<script>
    $wire.on('statuses-count-updated', (statusesCount) => {
        $(document).ready(function() {
            var table = $('#datatable').DataTable({
                "lengthMenu": [[1, 10, 20, 50], [1, 10, 20, 50]],
                "pageLength": 10,
                "searching": true,
                "paging": true,
                "pagingType": "simple_numbers",
                "order": [[0, 'desc']],
            });

            var currentMonth = $('#month').val().toLowerCase();
            var currentYear = $('#year').val().toLowerCase();

            table.column(0).search(currentYear).draw();
            table.column(0).search(currentMonth).draw();

            $('#status').on('change', function() {
                var status = $(this).val().toLowerCase();
                if (status === "all") {
                    table.column(3).search('').draw();
                } else {
                    table.column(3).search(status).draw();
                }
            });

            $('#year').on('change', function() {
                var year = $(this).val().toLowerCase();
                table.column(0).search(year).draw();
            });

            $('#month').on('change', function() {
                var month = $(this).val().toLowerCase();
                table.column(0).search(month).draw();
            });
        });
    });
</script>
@endscript
