<div class="content-wrapper">
    <div id="home" class="h-screen">
        <div class="container mx-auto px-4 py-8 h-full flex flex-col md:flex-row items-center md:mt-[-100px]">
            <div class="w-full md:w-1/2 animate__animated animate__fadeInLeft animate__delay-1s">
                <header>
                    <p class="text-center text-xl text-white md:text-left">Selamat datang di Skillee</h3>
                    <h1 class="text-2xl text-center text-white font-bold mt-3 md:text-4xl md:text-left" style="text-shadow: 3px 3px 0px rgba(0, 0, 139, 0.5);">
                        <span class="text-[#7FDEFF]">Mulai Karir Anda dengan </span><span class="text-[#fd4e4f]">Skillee</span>
                        <br/>Peluang Magang untuk Masa Depan Anda
                    </h1>
                    <p class="mt-4 text-md text-white text-center md:text-left">Buka pintu menuju masa depan yang cerah dan penuh peluang dengan Skillee!</p>
                </header>
                <main class="mt-4">
                    <div class="flex justify-center items-center mt-6 md:justify-start">
                        <a href="{{ url('/choose-registration') }}" wire:navigate class="inline-block bg-blue-600 hover:bg-blue-700 text-white font-semibold px-5 py-2 rounded-md shadow-md">Daftar Sekarang</a>
                    </div>
                </main>
            </div>
            <div id="image-carousel" class="splide w-full md:w-1/2 flex justify-center order-first md:order-last animate__animated animate__fadeInRight animate__delay-0.5s">
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide">
                            <img src="{{ asset('img/splide1.png') }}" alt="Image 1" class="max-w-full h-auto">
                        </li>
                        <li class="splide__slide">
                            <img src="{{ asset('img/splide2.png') }}" alt="Image 2" class="max-w-full h-auto">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <section>
            <div class="wave wave1"></div>
            <div class="wave wave2"></div>
            <div class="wave wave3"></div>
        </section>
    </div>
    <div id="about" class="bg-white flex items-center mt-[-85px] justify-center">
        <div class="container mx-auto px-4 py-8 h-full flex flex-col md:flex-row items-center animate-section">
            <div class="w-full flex justify-center mt-8 md:w-1/3 md:mt-0 animate__animated" id="about-image">
                <img src="{{ asset('img/about-left-image.png') }}" alt="Your Image" class="max-w-full h-auto">
            </div>
            <div class="w-full md:w-2/3 px-4 py-8 md:pl-12">
                <div class="grid grid-cols-1 md:grid-cols-2 gap-8 text-[#212529]">
                    <div class="flex items-center bg-blue-200 rounded-lg p-4 md:p-0 md:border-none md:bg-transparent animate__animated">
                        <img src="{{ asset('img/icon-karir.png') }}" alt="Icon 1" class="w-16 h-16 mr-4">
                        <div>
                            <h2 class="text-lg font-bold">Pengembangan Karir</h2>
                            <p>Tingkatkan keterampilan dan karir Anda dengan program kami.</p>
                        </div>
                    </div>
                    <div class="flex items-center bg-blue-200 rounded-lg p-4 md:p-0 md:border-none md:bg-transparent animate__animated">
                        <img src="{{ asset('img/icon-magang.png') }}" alt="Icon 2" class="w-16 h-16 mr-4">
                        <div>
                            <h2 class="text-lg font-bold">Peluang Magang</h2>
                            <p>Raih pengalaman kerja nyata dengan magang di berbagai bidang.</p>
                        </div>
                    </div>
                    <div class="flex items-center bg-blue-200 rounded-lg p-4 md:p-0 md:border-none md:bg-transparent animate__animated">
                        <img src="{{ asset('img/icon-jaringan.png') }}" alt="Icon 3" class="w-16 h-16 mr-4">
                        <div>
                            <h2 class="text-lg font-bold">Jaringan Profesional</h2>
                            <p>Perluas jaringan dan koneksi profesional Anda.</p>
                        </div>
                    </div>
                    <div class="flex items-center bg-blue-200 rounded-lg p-4 md:p-0 md:border-none md:bg-transparent animate__animated">
                        <img src="{{ asset('img/icon-pelatihan.png') }}" alt="Icon 4" class="w-16 h-16 mr-4">
                        <div>
                            <h2 class="text-lg font-bold">Pelatihan Keterampilan</h2>
                            <p>Ikuti pelatihan keterampilan untuk meningkatkan kompetensi Anda.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="contact" class="bg-cover bg-center md:h-[800px] py-16" style="background-image: url('{{ asset('img/contact-bg.png') }}');">
        <div class="container mx-auto px-4 md:mt-[80px]">
            <div class="flex flex-col md:flex-row p-8">
                <div class="text-white md:w-1/2 mb-8 md:mb-0 flex flex-col justify-center animate__animated">
                    <h3 class="mb-4 text-3xl font-bold md:mr-8">Jika Anda memiliki pertanyaan, jangan ragu untuk menghubungi kami</h3>
                    <p class="mb-4 text-md md:mr-8">Untuk pertanyaan lebih lanjut, anda dapat menghubungi kami melalui.</p>
                    <p class="text-lg font-semibold">Telepon: +62 123 456 789</p>
                    <p class="text-lg font-semibold">Email: cs@telkom.com</p>
                </div>
                <div class="md:w-1/2 animate__animated">
                        @if (session()->has('success'))
                            <div class="bg-green-500 text-white p-4 rounded mb-4">
                                Pesan anda telah terkirim.
                            </div>
                        @endif

                        @if (session()->has('error'))
                            <div class="bg-red-500 text-white p-4 rounded mb-4">
                                Tolong tunggu selama 5 menit sebelum mengirim pesan lagi.
                            </div>
                        @endif
                    <form wire:submit.prevent="sendMessage" class="bg-white p-6 rounded-lg shadow-lg">
                        <div class="mb-4 md:mt-6">
                            <label for="name" class="block text-sm font-medium text-gray-700">Nama</label>
                            <input wire:model="name" type="text" id="name" name="name" class="mt-1 block w-full border border-blue-300 rounded-lg shadow-sm py-2 px-3 focus:outline-none">
                            @error('name') <span class="text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="mb-4">
                            <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                            <input wire:model="email" type="email" id="email" name="email" class="mt-1 block w-full border border-blue-300 rounded-lg shadow-sm py-2 px-3 sm:text-sm">
                            @error('email') <span class="text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="mb-4">
                            <label for="message" class="block text-sm font-medium text-gray-700">Pesan</label>
                            <textarea wire:model="message" id="message" name="message" rows="4" class="mt-1 block w-full border border-blue-300 rounded-lg shadow-sm py-2 px-3 sm:text-sm"></textarea>
                            @error('message') <span class="text-red-500">{{ $message }}</span> @enderror
                        </div>
                        <div class="md:mb-6">
                            <button type="submit" class="w-full bg-blue-600 hover:bg-blue-700 text-white font-semibold py-2 px-4 rounded-lg">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>