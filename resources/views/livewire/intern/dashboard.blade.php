<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Intern Job List</h1>
    <div class="grid grid-cols-1 gap-6">
        @foreach($dataJobs as $jobs)
        <div class="bg-white rounded-md shadow-md p-6">
            <h2 class="text-lg font-semibold mb-2">{{ $jobs->name }}</h2>
            <p class="text-gray-600"><span class="font-semibold">Cabang:</span> {{ $jobs->branch_name }}</p>
            <p class="text-gray-600"><span class="font-semibold">Deskripsi:</span> {{ $jobs->description }}</p>
            <p class="text-gray-600"><span class="font-semibold">Kuota:</span> {{ $jobs->quota }}</p>
            <p class="text-gray-600"><span class="font-semibold">Batas Waktu:</span> {{ $jobs->deadline }}</p>
            <p class="text-gray-600"><span class="font-semibold">Pelamar:</span> {{ $jobs->applicator }}</p>
            <a href="{{ url('/internship/apply') }}/{{ $jobs->id }}" class="mt-4 inline-block px-4 py-2 bg-blue-500 hover:bg-blue-600 text-white font-semibold rounded">Apply</a>

        </div>
        @endforeach
    </div>
</main>