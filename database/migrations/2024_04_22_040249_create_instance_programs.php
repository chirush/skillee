<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('instance_programs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('instance_id')->unsigned()->references('id')->on('instances')->onDelete('cascade')->nullable();
            $table->foreignId('faculty_id')->unsigned()->references('id')->on('instance_faculties')->onDelete('cascade')->nullable();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('instance_programs');
    }
};
