<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('interns', function (Blueprint $table) {
            $table->id();
            $table->foreignId('instructor_id')->unsigned()->nullable()->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('branch_id')->unsigned()->nullable()->references('id')->on('branches')->onDelete('cascade');
            $table->string('instance');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone', 15)->unique();
            $table->string('nik');
            $table->string('domicile');
            $table->enum('level', ['High School', 'University']);
            $table->string('picture')->nullable();
            $table->string('verification_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('interns');
    }
};
