<?php

namespace App\Livewire\Homepage;

use Livewire\Component;

class ChooseRegistration extends Component
{
    public function render()
    {
        return view('livewire.homepage.choose-registration')->layout('livewire.layout.choose-registration');
    }
}
