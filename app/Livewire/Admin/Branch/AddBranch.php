<?php

namespace App\Livewire\Admin\Branch;

use Livewire\Component;
use App\Models\Branch;
use App\Models\Location;

class AddBranch extends Component
{
    public $name;
    public $contact;
    public $address;
    public $latitude;
    public $longitude;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'name' => 'required|unique:branches,name',
            'contact' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);
    }

    public function addBranch()
    {
        $validatedData = $this->validate([
            'name' => 'required|unique:branches,name',
            'contact' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        $branch = Branch::create([
            'name' => $this->name,
            'contact' => $this->contact,
            'address' => $this->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ]);

        return redirect('/admin/branches');

    }
    public function render()
    {
        return view('livewire.admin.branch.add-branch', [
        ])->layout('livewire.layout.admin');
    }
}
