<div class="w-1/1 md:w-2/3 p-8 h-full overflow-y-auto">
    <h2 class="text-2xl font-bold mb-4 text-gray-700 md:text-3xl">Masuk untuk mengakses program magang kami!</h2>
    <form wire:submit.prevent="processLogin">
        @if (session('verificationMessage'))
            <div class="text-green-600 text-base">{{ session('verificationMessage') }}</div>
        @endif
        @if($verificationError)
            <div class="text-red-600 text-base">{{ $verificationError }}. <a href="{{ route('resend.verification.email', ['email' => $email]) }}" class="text-blue-500">Kirim email verifikasi</a> </div>
            
        @elseif($loginError)
            <div class="text-red-600 text-base">{{ $loginError }}</div>
        @endif
        <div class="mb-4">
            <label for="email" class="block text-gray-700 font-semibold mb-2">Email</label>
            <input type="email" id="email" name="email" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan email anda" wire:model.lazy="email">
            @error('email')
                <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-4">
            <label for="password" class="block text-gray-700 font-semibold mb-2">Password</label>
            <div class="relative">
                <input type="password" id="password" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500" placeholder="Masukkan password anda" wire:model.lazy="password">
                <button type="button" id="password-toggle" class="absolute top-0 right-0 mr-4 mt-3 text-sm text-gray-600 focus:outline-none" onclick="togglePasswordVisibility('password')">
                    <i class="far fa-eye" id="password-icon"></i>
                </button>
            </div>
            @error('password')
                <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
            @enderror
        </div>
        <div class="mb-4">
            <div id="captcha" wire:ignore></div>
            @error('captcha')
                <span class="text-red-600" style="font-size: 12.5px;">{{ $message }}</span>
            @enderror
        </div>

        <button type="submit" class="w-full bg-blue-500 text-white py-2 rounded-lg hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Login</button>
        <div class="mt-4 text-sm text-gray-600">
            Belum memiliki akun? <a href="{{ url('/choose-registration') }}" wire:navigate class="text-blue-500">Daftar disini</a>
        </div>
    </form>
</div>

<script>
    function togglePasswordVisibility() {
        var passwordInput = document.getElementById("password");
        var passwordIcon = document.getElementById("password-icon");

        if (passwordInput.type === "password") {
            passwordInput.type = "text";
            passwordIcon.classList.remove("fa-eye");
            passwordIcon.classList.add("fa-eye-slash");
        } else {
            passwordInput.type = "password";
            passwordIcon.classList.remove("fa-eye-slash");
            passwordIcon.classList.add("fa-eye");
        }
    }

    var  handle = function(e) {
        widget = grecaptcha.render('captcha', {
            'sitekey': '{{ env('CAPTCHA_SITE_KEY') }}',
            'theme': 'light',
            'callback': verify
        });
 
    }
    var verify = function (response) {
        @this.set('captcha', response)
    }
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=handle&render=explicit"
    async
    defer>
</script>