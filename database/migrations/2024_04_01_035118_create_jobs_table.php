<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('branch_id')->unsigned()->references('id')->on('branches')->onDelete('cascade');
            $table->string('name');
            $table->string('description');
            $table->string('quota');
            $table->date('date_start');
            $table->date('deadline');
            $table->enum('level', ['High School', 'University']);
            $table->string('applicator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jobs');
    }
};
