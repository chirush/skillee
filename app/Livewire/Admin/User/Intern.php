<?php

namespace App\Livewire\Admin\User;

use Livewire\Component;
use App\Models\Intern as Interns;
use Illuminate\Support\Facades\Auth;

class Intern extends Component
{
    public $dataInterns;

    public function mount()
    {
        $user = Auth::user();
        $query = Interns::join('branches', 'interns.branch_id', '=', 'branches.id')
            ->join('users', 'interns.instructor_id', '=', 'users.id')
            ->join('instances', 'interns.instance_id', '=', 'instances.id')
            ->select('interns.*', 'users.name as instructor_name', 'branches.name as branch_name', 'instances.name as instance_name');

        if ($user->role == "Instructor") {
            $query->where('interns.instructor_id', $user->id);
        } elseif ($user->role == "Human Resources") {
            $query->where('interns.branch_id', $user->branch_id);
        }

        $this->dataInterns = $query->get();

    }

    public function render()
    {
        return view('livewire.admin.user.intern', [
            'dataInterns' => $this->dataInterns,
        ])->layout('livewire.layout.admin');
    }
}
