<?php

namespace App\Livewire\Admin\Monitor;

use Livewire\Component;
use App\Models\Attendance as Attendances;
use Illuminate\Support\Facades\Auth;

class Attendance extends Component
{
    public $dataAttendances;

    public function mount()
    {
        $user = Auth::user();
        $query = Attendances::join('interns', 'attendances.intern_id', '=', 'interns.id')
            ->join('branches', 'interns.branch_id', '=', 'branches.id')
            ->select('attendances.*', 'interns.name as intern_name', 'branches.name as branch_name');

        if ($user->role == "Instructor") {
            $query->where('interns.instructor_id', $user->id);
        } elseif ($user->role == "Human Resources") {
            $query->where('interns.branch_id', $user->branch_id);
        }

        $this->dataAttendances = $query->get();
    }

    public function render()
    {
        return view('livewire.admin.monitor.attendance', [
            'dataAttendances' => $this->dataAttendances,
        ])->layout('livewire.layout.admin');
    }
}
