<?php

namespace App\Livewire\Intern\Job;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Jobs;
use App\Models\Application;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Job extends Component
{
    use WithPagination;

    public $selectedJob;
    public $selectedJobId;
    public $pendingApplication;
    public $internLevel;
    public $searchTerm;
    public $selectedBranch;

    public function mount()
    {
        $intern = Auth::guard('intern')->user();

        if ($intern->branch_id && $intern->date_start <= now()) {
            return redirect('internship/attendances');
        }

        $this->internLevel = Auth::guard('intern')->user()->level;

        $this->loadDataJobs();

        if (session()->has('selectedJobId')) {
            $this->selectedJobId = session('selectedJobId');
            $this->selectedJob = $this->getJobDetails($this->selectedJobId);
        } else {
            $this->selectedJob = $this->dataJobs->first();
            if ($this->selectedJob) {
                $this->selectedJobId = $this->selectedJob->id;
                $this->formatJobDates($this->selectedJob);
            }
        }

        $internId = Auth::guard('intern')->user()->id;

        $this->pendingApplication = Application::where('intern_id', $internId)
            ->where('status', '!=' , "Declined")
            ->count();
    }

    public function loadDataJobs()
    {
        $today = date('Y-m-d');

        $query = Jobs::join('branches', 'jobs.branch_id', '=', 'branches.id')
            ->where('level', $this->internLevel)
            ->where('jobs.date_start', '<=', $today)
            ->where('jobs.deadline', '>=', $today);

        if (!empty($this->searchTerm)) {
            $query->where(function($q) {
                $q->where('jobs.name', 'like', '%' . $this->searchTerm . '%')
                    ->orWhere('jobs.description', 'like', '%' . $this->searchTerm . '%');
            });
        }

        if (!empty($this->selectedBranch)) {
            $query->where('jobs.branch_id', $this->selectedBranch);
        }

        $this->dataJobs = $query->select('jobs.*', 'branches.name as branch_name')
            ->paginate(5);
    }

    public function selectJob($jobId)
    {
        $this->selectedJobId = $jobId;
        $this->selectedJob = $this->getJobDetails($jobId);

        session()->put('selectedJobId', $jobId);
    }

    public function applyForJob($jobId)
    {
        $internId = Auth::guard('intern')->user()->id;

        if (!Auth::guard('intern')->user()->resume || !Auth::guard('intern')->user()->mentor_email || !Auth::guard('intern')->user()->bio) {
            session()->flash('error', 'Harap lengkapi data diri anda sebelum melamar pekerjaan.');
            return redirect('/internship/profile');
        }

        Application::create([
            'intern_id' => $internId,
            'job_id' => $jobId,
            'application_date' => now(),
            'status' => 'Pending',
        ]);

        if ($this->selectedJob) {
            $this->selectedJob->increment('applicator');
        }

        return redirect('/internship/jobs')->with('status', 'Anda telah berhasil melamar pekerjaan.');
    }

    public function updatingPage()
    {
        if ($this->selectedJobId) {
            $this->selectedJob = $this->getJobDetails($this->selectedJobId);
        }
    }

    public function getJobDetails($jobId)
    {
        $job = Jobs::join('branches', 'jobs.branch_id', '=', 'branches.id')
            ->where('jobs.id', $jobId)
            ->select('jobs.*', 'branches.name as branch_name', 'branches.address', 'branches.contact')
            ->first();

        if ($job) {
            $this->formatJobDates($job);
        }

        return $job;
    }

    public function formatJobDates(&$job)
    {
        $job->periode_start_formatted = Carbon::parse($job->periode_start)->isoFormat('D MMMM', 'Do MMMM');
        $job->periode_end_formatted = Carbon::parse($job->periode_end)->isoFormat('D MMMM YYYY', 'Do MMMM YYYY');
        $job->deadline_formatted = Carbon::parse($job->deadline)->isoFormat('D MMMM YYYY', 'Do MMMM YYYY');
    }

    public function search()
    {
        $this->resetPage();

        $this->loadDataJobs();
    }

    public function render()
    {
        
        $this->loadDataJobs();
        return view('livewire.intern.job.job', [
            'dataJobs' => $this->dataJobs,
            'selectedJob' => $this->selectedJob,
            'pendingApplication' => $this->pendingApplication,
        ])->layout('livewire.layout.internship');
    }
}
