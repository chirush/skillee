<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;
    protected $table = 'attendances';
    protected $primaryKey = 'id';

    protected $fillable = [
        'intern_id', 'status', 'description', 'latitude', 'longitude', 'picture', 'date', 'time'
    ];
}
