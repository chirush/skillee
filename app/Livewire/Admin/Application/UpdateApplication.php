<?php

namespace App\Livewire\Admin\Application;

use Livewire\Component;
use App\Models\Application;
use App\Models\Intern;
use App\Models\Jobs;
use App\Models\User;
use App\Models\Branch;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;

class UpdateApplication extends Component
{
    public $status;
    public $instructor;
    public $applicationId;
    public $dataIntern;
    public $dataJobs;
    public $dataBranches;
    public $dataInstructor;
    public $formattedDomicile;

    public function updated($field)
    {
        $this->validateOnly($field, [
            'status' => 'required',
        ]);
    }

    public function mount($id)
    {
        $this->applicationId = $id;

        $application = Application::findOrFail($id);

        $this->dataIntern = Intern::join('instances', 'interns.instance_id', '=', 'instances.id')
        ->where('interns.id', $application->intern_id)
        ->select('interns.*', 'instances.name as instance_name')
        ->first();

        $this->dataJobs = Jobs::where('id', $application->job_id)->first();

        $this->dataBranches = Branch::where('id', $this->dataJobs->branch_id)->first();

        $this->dataInstructor = User::where('role', "instructor")->get();

        list($provinceId, $regencyId, $districtId) = explode(',', $this->dataIntern->domicile);

        $province = ucwords(strtolower(Province::find($provinceId)->name));
        $regency = ucwords(strtolower(Regency::find($regencyId)->name));
        $district = ucwords(strtolower(District::find($districtId)->name));

        $this->formattedDomicile = $province . ", " . $regency . ", " . $district;
    }

    public function updateApplication()
    {

        $dbApplication = Application::findOrFail($this->applicationId);

        $validatedData = $this->validate([
            'status' => 'required',
        ]);

        $application = [
            'status' => $this->status,
        ];

        $dbJob = Jobs::findOrFail($dbApplication->job_id);

        $dbApplication->update($application);

        if($this->status == "Approved"){
            $dbIntern = Intern::findOrFail($dbApplication->intern_id);

            $intern = [
                'branch_id' => $dbJob->branch_id,
                'instructor_id' => $this->instructor,
                'position' => $dbJob->name,
                'date_start' => $dbJob->periode_start,
                'date_end' => $dbJob->periode_end,
            ];

            $dbIntern->update($intern);
        }

        return redirect('/admin/applications');
    }

    public function render()
    {
        return view('livewire.admin.application.update-application', [
            'dataIntern' => $this->dataIntern,
            'dataJobs' => $this->dataJobs,
            'dataBranches' => $this->dataBranches,
            'dataInstructor' => $this->dataInstructor,
        ])->layout('livewire.layout.admin');
    }
}
