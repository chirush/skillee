<main class="flex-1 p-6">
    <div class="grid grid-cols-1 lg:grid-cols-3 gap-6 mb-6">
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-blue-500">
                <i class="fas fa-envelope text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Messages</h3>
                <p class="text-3xl font-bold text-gray-800">24</p>
            </div>
        </div>
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-green-500">
                <i class="fas fa-file-alt text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Applications</h3>
                <p class="text-3xl font-bold text-gray-800">10</p>
            </div>
        </div>
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-yellow-500">
                <i class="fas fa-bell text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Notifications</h3>
                <p class="text-3xl font-bold text-gray-800">5</p>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-1 lg:grid-cols-3 gap-6 mb-6">
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-purple-500">
                <i class="fas fa-tasks text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Tasks</h3>
                <p class="text-3xl font-bold text-gray-800">12</p>
            </div>
        </div>
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-indigo-500">
                <i class="fas fa-check text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Completed Tasks</h3>
                <p class="text-3xl font-bold text-gray-800">8</p>
            </div>
        </div>
        <div class="bg-white rounded-md shadow-md p-4 flex items-center">
            <div class="flex-shrink-0 text-red-500">
                <i class="fas fa-clock text-3xl"></i>
            </div>
            <div class="ml-4">
                <h3 class="text-lg font-semibold text-gray-800">Pending Tasks</h3>
                <p class="text-3xl font-bold text-gray-800">4</p>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-1 gap-6 mb-6">
        <div class="bg-white rounded-md shadow-md p-6">
            <h2 class="text-xl font-semibold text-gray-800">Recent Activities</h2>
            <ul class="mt-4 text-sm text-gray-600">
                <li class="mb-2">You received a new message from John Doe.</li>
                <li class="mb-2">Your application for the Marketing Manager position has been approved.</li>
                <li class="mb-2">New task assigned: Complete the monthly report.</li>
                <li class="mb-2">You have a new notification.</li>
            </ul>
        </div>
    </div>
</main>