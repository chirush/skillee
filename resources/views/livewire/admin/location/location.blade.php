<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Locations</h1>
    <div class="mb-6">
        <a href="{{ url('/admin/locations/add') }}" wire:navigate class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Add Location</a>
    </div>
    <div class="overflow-x-auto">
        <table id="example" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Latitude</th>
                    <th class="px-4 py-2">Longitude</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataLocations as $locations)
                <tr>
                    <td class="border px-4 py-2">{{ $locations->id }}</td>
                    <td class="border px-4 py-2">{{ $locations->latitude }}</td>
                    <td class="border px-4 py-2">{{ $locations->longitude }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ url('/admin/locations/edit/') }}/{{ $locations->id }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Edit</a>
                        <button wire:click="deleteLocation({{ $locations->id }})" class="text-red-700 hover:text-red-900">Delete</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@push('scripts')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endpush
