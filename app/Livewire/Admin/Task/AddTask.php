<?php

namespace App\Livewire\Admin\Task;

use Livewire\Component;
use App\Models\Task;
use App\Models\Intern;
use Illuminate\Support\Facades\Auth;

class AddTask extends Component
{
    public $dataInterns;
    public $intern;
    public $title;
    public $description;
    public $deadline;

    public function mount()
    {        
        $this->dataInterns = Intern::where('instructor_id', Auth::user()->id)->get();  
    }

    public function updated($field)
    {
        $this->validateOnly($field, [
            'intern' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date',
        ]);
    }

    public function addTask()
    {
        $validatedData = $this->validate([
            'intern' => 'required',
            'title' => 'required',
            'description' => 'required',
            'deadline' => 'required|date',
        ]);

        $task = Task::create([
            'intern_id' => $this->intern,
            'title' => $this->title,
            'description' => $this->description,
            'deadline' => $this->deadline,
        ]);

        return redirect('/admin/tasks');
    }
    
    public function render()
    {
        return view('livewire.admin.task.add-task', [
            'dataInterns' => $this->dataInterns,
        ])->layout('livewire.layout.admin');
    }
}
