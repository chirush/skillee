<div class="flex justify-center items-center h-screen mt-[-100px]">
    <div>
        <div class="text-center mb-8">
            <h1 class="text-3xl font-bold text-gray-700 md:text-4xl">Selamat Datang di Program Magang <span class="text-[#fd4e4f]">Skillee</span>!</h1>
            <p class="text-gray-600 text-md mt-2 md:text-lg">Pilih program yang sesuai dengan status pendidikan Anda.</p>
        </div>

        <div class="flex justify-center items-center">
            <div class="max-w-lg w-full ml-4 bg-white rounded-lg overflow-hidden text-center border-l border-t border-blue-500 border-opacity-30" style="box-shadow: 7px 7px 4px rgba(0, 0, 139, 0.5);">
                <div class="flex justify-center items-center mt-4">
                    <img src="{{ asset('img/student.png') }}" class="h-[150px]">
                </div>
                <div class="p-4">
                    <h2 class="text-lg font-semibold text-gray-700 md:text-xl">Siswa</h2>
                    <p class="text-gray-600 mt-2 text-md md:text-lg">Dapatkan pengalaman magang pertama Anda dan kembangkan keterampilan dasar yang diperlukan di dunia kerja.</p>
                </div>
                <a href="{{ url('/register?program=high-school') }}">
                    <div class="bg-blue-500 hover:bg-blue-600 text-white transition duration-300 px-4 py-3 flex justify-center items-center">
                        <button class="text-sm font-semibold text-white focus:outline-none">Daftar Sekarang</button>
                    </div>
                </a>
            </div>

            <div class="max-w-lg ml-4 mr-4 w-full bg-white rounded-lg overflow-hidden text-center border-l border-t border-blue-500 border-opacity-30" style="box-shadow: 7px 7px 4px rgba(0, 0, 139, 0.5);">
                <div class="flex justify-center items-center mt-4">
                    <img src="{{ asset('img/student.png') }}" class="h-[150px]">
                </div>
                <div class="p-4">
                    <h2 class="text-lg font-semibold text-gray-700 md:text-xl">Mahasiswa</h2>
                    <p class="text-gray-600 mt-2 text-md md:text-lg">Perdalam pengetahuan dan keterampilan Anda melalui program magang yang sesuai dengan bidang studi Anda.</p>
                </div>
                <a href="{{ url('/register?program=university') }}">
                    <div class="bg-blue-500 hover:bg-blue-600 text-white transition duration-300 px-4 py-3 flex justify-center items-center">
                        <button class="text-sm font-semibold text-white focus:outline-none">Daftar Sekarang</button>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
