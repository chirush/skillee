<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Message</h1>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">Date</th>
                    <th class="px-4 py-2">Name</th>
                    <th class="px-4 py-2">Message</th>
                    <th class="px-4 py-2">Email</th>
                    <th class="px-4 py-2">Status</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataMessages->reverse() as $messages)
                <tr>
                    <td class="border px-4 py-2">{{ $messages->created_at }}</td>
                    <td class="border px-4 py-2">{{ $messages->name }}</td>
                    <td class="border px-4 py-2">{{ $messages->message }}</td>
                    <td class="border px-4 py-2">{{ $messages->email }}</td>
                    <td class="border px-4 py-2">{{ $messages->status }}</td>
                    <td class="border px-4 py-2">
                        @if ($messages->status == "Unread")
                        <a href="{{ url('/admin/messages/mark/') }}/{{ $messages->id }}" class="text-red-700 hover:text-red-900">Mark as Read</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });
    });
</script>
@endscript