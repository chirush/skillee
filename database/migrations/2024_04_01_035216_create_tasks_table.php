<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('intern_id')->unsigned()->references('id')->on('interns')->onDelete('cascade');
            $table->string('title');
            $table->string('description');
            $table->string('deadline');
            $table->enum('status', ['In Progress', 'Under Review', 'Completed', 'Revised']);
            $table->string('report')->nullable();
            $table->string('file')->nullable();
            $table->string('picture')->nullable();
            $table->string('revise_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
