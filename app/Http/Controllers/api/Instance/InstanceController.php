<?php

namespace App\Http\Controllers\api\Instance;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Instance;
use App\Http\Resources\FormatPostResource;

class InstanceController extends Controller
{
    public function getInstance()
    {
        $dataInstance = Instance::all();

        return [
            'dataInstance' => $dataInstance,
        ];
    }

    public function createInstance(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique:instances,name',
            'description' => 'required',
        ]);

        $user = Instance::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
    }

    public function deleteInstance($id)
    {
        $dataInstance = Instance::findOrFail($id);
        $dataInstance->delete();
    }
}
