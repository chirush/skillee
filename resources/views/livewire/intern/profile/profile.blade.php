<main class="flex-1 p-6">
    <div class="container mx-auto">
        <div class="w-full bg-white shadow-md rounded-lg overflow-hidden">
            <div class="bg-gray-200 px-6 py-4 flex items-center justify-between">
                <div>
                    <h2 class="text-xl font-semibold text-gray-800">{{ Auth::guard('intern')->user()->name }}</h2>
                    @if(!Auth::guard('intern')->user()->position)
                    <p class="text-sm text-gray-600">Applicant</p>
                    @else
                    <p class="text-sm text-gray-600">{{ Auth::guard('intern')->user()->position }}</p>
                    <p class="text-sm text-gray-600">{{ DB::table('branches')->where('id', Auth::guard('intern')->user()->branch_id)->value('name') }}</p>
                    @endif
                </div>
                <div class="flex items-center">
                    @if(!Auth::guard('intern')->user()->picture)
                    <img src="https://www.gravatar.com/avatar/?d=mp" alt="Profile Picture" class="w-12 h-12 rounded-full">
                    @else
                    <img src="{{ asset('storage/picture') }}/{{ Auth::guard('intern')->user()->picture }}" alt="Profile Picture" class="w-12 h-12 rounded-full">
                    @endif
                    <form id="pictureForm" action="{{ url('/upload-picture') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="ml-4">
                            <label for="pictureInput" class="cursor-pointer text-sm text-blue-600">Ubah</label>
                            <input type="file" id="pictureInput" name="picture" class="hidden" accept=".jpg, .png, .jpeg">
                        </div>
                    </form>
                </div>
            </div>

            <div class="flex">
                <div class="w-2/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">Kontak Anda</h3>
                        <button id="editYourContactBtn" class="text-sm text-blue-500 hover:underline" onclick="toggleYourContactForm()">Edit</button>
                    </div>
                    <form wire:submit.prevent="updateYourContact" id="yourContactForm">
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Nama</span><br/>
                            <span id="yourNameDisplay">{{ Auth::guard('intern')->user()->name }}</span>
                            <input id="yourNameForm" type="text" name="name" wire:model="name" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Email</span><br/>
                            <span id="yourEmailDisplay">{{ Auth::guard('intern')->user()->email }}</span>
                            <input id="yourEmailForm" type="email" name="email" wire:model="email" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden" disabled>
                        </p>
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Phone</span><br/>
                            <span id="yourPhoneDisplay">{{ Auth::guard('intern')->user()->phone }}</span>
                            <input id="yourPhoneForm" type="text" name="phone" wire:model="phone" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <button id="saveYourContactBtn" type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg hidden">Save</button>
                    </form>
                </div>
                <div class="w-1/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">
                            Kontak Pembimbing
                            @if (!Auth::guard('intern')->user()->mentor_email || !Auth::guard('intern')->user()->mentor_phone)
                            <span class="text-red-500">*</span>
                            @endif
                        </h3>
                        <button id="editMentorContactBtn" class="text-sm text-blue-500 hover:underline" onclick="toggleMentorContactForm()">Edit</button>
                    </div>
                    <form wire:submit.prevent="updateMentorContact" id="mentorContactForm">
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Nama</span><br/>
                            <span id="mentorNameDisplay">{{ Auth::guard('intern')->user()->mentor_name ?: '-' }}</span>
                            <input id="mentorNameForm" type="text" name="mentor_name" wire:model="mentor_name" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Email</span><br/>
                            <span id="mentorEmailDisplay">{{ Auth::guard('intern')->user()->mentor_email ?: '-' }}</span>
                            <input id="mentorEmailForm" type="email" name="mentor_email" wire:model="mentor_email" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Phone</span><br/>
                            <span id="mentorPhoneDisplay">{{ Auth::guard('intern')->user()->mentor_phone ?: '-' }}</span>
                            <input id="mentorPhoneForm" type="text" name="mentor_phone" wire:model="mentor_phone" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <button id="saveMentorContactBtn" type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg hidden">Save</button>
                    </form>
                </div>
            </div>
            <div class="flex">
                <div class="w-2/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">Informasi Pribadi</h3>
                        <button id="editPersonalInfoBtn" class="text-sm text-blue-500 hover:underline" onclick="togglePersonalInfoForm()">Edit</button>
                    </div>
                    <form wire:submit.prevent="updatePersonalInfo" id="personalInfoForm">
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">NIK</span><br/>
                            <span id="nikDisplay">{{ Auth::guard('intern')->user()->nik }}</span>
                            <input id="nikForm" type="text" name="nik" wire:model="nik" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        </p>
                        <p class="text-gray-600 mb-2">
                            <span class="font-semibold">Jenis Kelamin</span><br/>
                            <span id="genderDisplay">{{ Auth::guard('intern')->user()->gender ?: '-' }}</span>
                            <select id="genderForm" name="gender" wire:model="gender" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                                <option value="null" selected disabled>{{ __('Pilih Jenis Kelamin') }}</option>
                                <option value="Laki-Laki">Laki-Laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </p>
                        <p class="text-gray-600">
                            <span class="font-semibold">Domisili</span><br/>
                            <span id="domicileDisplay">{{ $formattedDomicile }}</span>
                            <input id="domicileForm" type="text" name="domicile" wire:model="formattedDomicile" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden" disabled>
                        </p>
                        <button id="savePersonalInfoBtn" type="submit" class="bg-gray-800 mt-2 hover:bg-gray-900 text-white py-2 px-4 rounded-lg hidden">Save</button>
                    </form>
                </div>
                <div class="w-1/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">Pendidikan</h3>
                        <button id="editEducationBtn" class="text-sm text-blue-500 hover:underline" onclick="toggleEducationForm()">Edit</button>
                    </div>
                    <form wire:submit.prevent="updateEducation">
                        <p class="text-gray-600 mb-2"><span class="font-semibold">
                        @if (Auth::guard('intern')->user()->level == "High School")
                        Sekolah</span><br/><span id="schoolDisplay">{{ DB::table('instances')->where('id', Auth::guard('intern')->user()->instance_id)->value('name') }}</span></p>
                        <input id="schoolForm" type="text" name="instance" wire:model="instance" class="w-full px-4 py-2 mb-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden" disabled>
                        <span id="facultyDisplay"></span></p>
                        <input id="facultyForm" type="hidden" value="-" name="faculty" class="w-full px-4 py-2 mb-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        @else
                        Universitas</span><br/><span id="schoolDisplay">{{ DB::table('instances')->where('id', Auth::guard('intern')->user()->instance_id)->value('name') }}</span></p>
                        <input id="schoolForm" type="text" name="instance" wire:model="instance" class="w-full px-4 py-2 mb-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden" disabled>
                        <p class="text-gray-600 mb-2"><span class="font-semibold">Fakultas</span><br/><span id="facultyDisplay">{{ Auth::guard('intern')->user()->faculty }}</span></p>
                        <input id="facultyForm" type="text" name="faculty" wire:model="faculty" class="w-full px-4 py-2 mb-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        @endif
                        <p class="text-gray-600"><span class="font-semibold">Jurusan</span><br/><span id="programDisplay">{{ Auth::guard('intern')->user()->program }}</span></p>
                        <input id="programForm" type="text" name="program" wire:model="program" class="w-full px-4 py-2 mt-2 border rounded-lg focus:outline-none focus:border-blue-500 hidden">
                        <button id="saveEducationBtn" type="submit" class="bg-gray-800 mt-2 hover:bg-gray-900 text-white py-2 px-4 rounded-lg hidden">Save</button>
                    </form>
                </div>
            </div>
            <div class="flex mb-4">
                <div class="w-2/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">
                            Biodata
                            @if (!Auth::guard('intern')->user()->bio)
                            <span class="text-red-500">*</span>
                            @endif
                        </h3>
                        <button id="editBioBtn" class="text-sm text-blue-500 hover:underline" onclick="toggleBioForm()">Edit</button>
                    </div>
                    <p id="bioDisplay" class="text-gray-600">{{ Auth::guard('intern')->user()->bio ?: '-' }}</p>
                    <form id="bioForm" wire:submit.prevent="updateBio" class="hidden">
                        <textarea name="bio" wire:model="bio" rows="4" class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:border-blue-500">{{ Auth::guard('intern')->user()->bio }}</textarea>
                        <button type="submit" class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">Save</button>
                    </form>
                </div>

                <div class="w-1/3 bg-white p-6">
                    <div class="mb-4 flex items-center justify-between">
                        <h3 class="text-lg font-semibold text-gray-800">
                            Resume
                            @if (!Auth::guard('intern')->user()->resume)
                            <span class="text-red-500">*</span>
                            @endif
                        </h3>
                        @if (Auth::guard('intern')->user()->resume)
                        <button id="editResumeBtn" class="text-sm text-blue-500 hover:underline" onclick="toggleResumeForm()">Edit</button>
                        @endif
                    </div>
                    <div id="resumeDisplay" class="{{ Auth::guard('intern')->user()->resume ? '' : 'hidden' }} mt-6">
                        @if (Auth::guard('intern')->user()->resume)
                            <a href="{{ asset('storage/resumes') }}/{{ Auth::guard('intern')->user()->resume }}" target="_blank" class="bg-gray-800 hover:bg-gray-700 text-white py-2 px-4 rounded">View Resume</a>
                        @endif
                    </div>
                    <form id="resumeForm" class="{{ Auth::guard('intern')->user()->resume ? 'hidden' : '' }}" action="{{ url('/upload-resume') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mt-[-10px]">
                            <input type="file" id="resumeInput" name="resume" class="border rounded-lg px-4 py-2 w-full" accept=".pdf">
                            @error('resume') <span class="text-red-500">{{ $message }}</span> @enderror
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<script src="{{ asset('js/profile.js') }}"></script>