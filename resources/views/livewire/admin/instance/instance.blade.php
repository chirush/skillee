<main class="flex-1 p-6">
    <h1 class="text-3xl font-semibold mb-6">Instances</h1>
    <div class="mb-6">
        <a href="{{ url('/admin/instances/add') }}" wire:navigate class="bg-gray-800 hover:bg-gray-900 text-white py-2 px-4 rounded-lg">+ Add Instance</a>
    </div>
    <div class="overflow-x-auto">
        <table id="datatable" class="table-auto min-w-full bg-white shadow-md rounded-lg overflow-hidden">
            <thead class="bg-gray-800 text-white">
                <tr>
                    <th class="px-4 py-2">ID</th>
                    <th class="px-4 py-2">Name</th>
                    <th class="px-4 py-2">Description</th>
                    <th class="px-4 py-2">Level</th>
                    <th class="px-4 py-2"></th>
                </tr>
            </thead>
            <tbody class="text-gray-700">
                @foreach($dataInstances as $instances)
                <tr>
                    <td class="border px-4 py-2">{{ $instances->id }}</td>
                    <td class="border px-4 py-2">{{ $instances->name }}</td>
                    <td class="border px-4 py-2">{{ $instances->description }}</td>
                    <td class="border px-4 py-2">{{ $instances->level }}</td>
                    <td class="border px-4 py-2">
                        <a href="{{ url('/admin/instances/edit/') }}/{{ $instances->id }}" wire:navigate class="text-blue-700 hover:text-blue-900 mr-2">Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</main>

@script
<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            "lengthMenu": [[5, 10, 20, 50], [5, 10, 20, 50]],
            "pageLength": 10,
            "searching": true,
            "paging": true,
            "pagingType": "simple_numbers",
            "order": [[0, 'desc']],
        });
    });
</script>
@endscript