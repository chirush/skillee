<?php

namespace App\Livewire\Admin\Application;

use Livewire\Component;
use App\Models\Application as Applications;
use Illuminate\Support\Facades\Auth;

class Application extends Component
{
    public $dataApplications;

    public function mount()
    {
        $user = Auth::user();
        $query = Applications::join('jobs', 'applications.job_id', '=', 'jobs.id')
            ->join('branches', 'jobs.branch_id', '=', 'branches.id')
            ->join('interns', 'applications.intern_id', '=', 'interns.id')
            ->select(
                'applications.*',
                'jobs.name as job_name',
                'branches.name as branch_name',
                'interns.name as intern_name',
                'interns.resume'
            );

        if ($user->role == "Human Resources") {
            $query->where('jobs.branch_id', $user->branch_id);
        }

        $this->dataApplications = $query->get();
    }

    public function deleteApplication($id)
    {
        Applications::find($id)->delete();
        $this->loadApplications();
    }

    public function render()
    {
        return view('livewire.admin.application.application', [
            'dataApplications' => $this->dataApplications,
        ])->layout('livewire.layout.admin');
    }
}
